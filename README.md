# Mi_PC.wav
## Un recorrido sonoro al interior de tu computadora

Este es el minijuego que arme como obra para Taller de Creación Sonora 2018.
Inicialmente fue creado en Pd-L2ork, pero para su difusión al público, fue
adaptado a Pd-Extended.

### Requerimientos
Este juego debería funcionar bien en cualquier OS que soporte Pd-Extended. Fue
probado en Windows 7 y Manjaro Linux.

Es altamente posible que la parte visual hecha en GEM no funcione en versiones
de OSX mayores o iguales a 10.11.

El objeto gcanvas, que esta version usa, no funciona en Pd-L2ork. Magias y misterios de Pd.

### Instalación
Bajar el zip o clonar a un directorio. Una vez hecho esto, descomprimir el zip
que contiene los samples en la misma carpeta que contiene los patches y las
imagenes. Todos los archivos tienen que quedar en el mismo nivel.

### Licencia
Tomen el código, usenlo, aprendan, lo que les pinte. No le cobren plata a nadie
por esto, no sean chantas :P

### Contacto
Pueden hablarme por facebook, o dejarme Issues o Merge Requests aca.
